import pandas as pd
import math
import graphviz

# Đọc dữ liệu từ CSV
def load_data(path):
    return pd.read_csv(path)

# Tính entropy của một tập nhãn
def entropy(labels):
    class_counts = labels.value_counts()
    entropy = 0
    total_samples = len(labels)
    for count in class_counts:
        prob = count / total_samples
        entropy -= prob * math.log2(prob)
    return entropy

# Tính Information Gain của một thuộc tính
def information_gain(data, attribute, target_attribute):
    entropy_total = entropy(data[target_attribute])
    attribute_values = data[attribute].unique()
    entropy_after_split = 0
    total_samples = len(data)
    for value in attribute_values:
        subset = data[data[attribute] == value]
        subset_entropy = entropy(subset[target_attribute])
        subset_weight = len(subset) / total_samples
        entropy_after_split += subset_weight * subset_entropy
    return entropy_total - entropy_after_split

# Xây dựng cây quyết định và vẽ bằng Graphviz
def build_and_draw_tree(data, target_attribute, attributes, node_name='Root', parent_value=None, graph=None):
    if graph is None:
        graph = graphviz.Digraph(comment='Decision Tree', format='png')
        graph.attr(rankdir='LR', size='10,5')  # Hướng từ trái sang phải, kích thước tổng thể
        graph.attr('node', shape='ellipse', style='filled', fillcolor='lightblue', fontname='Helvetica', fontsize='12')
        graph.attr('edge', fontname='Helvetica', fontsize='10')

    if len(data[target_attribute].unique()) == 1:
        # Trường hợp nút lá
        class_label = data[target_attribute].iloc[0]
        graph.node(node_name, f'{parent_value}\n{class_label}', shape='box', style='filled', fillcolor='lightgreen')
    else:
        # Tính Information Gain và chọn thuộc tính tốt nhất
        gains = {attr: information_gain(data, attr, target_attribute) for attr in attributes}
        best_attr = max(gains, key=gains.get)
        graph.node(node_name, f'{parent_value}\n{best_attr}' if parent_value else best_attr)
        attr_values = data[best_attr].unique()

        new_attrs = [attr for attr in attributes if attr != best_attr]
        for value in attr_values:
            child_name = f'{node_name}_{value}'
            graph.edge(node_name, child_name, label=str(value))
            subset = data[data[best_attr] == value]
            build_and_draw_tree(subset, target_attribute, new_attrs, node_name=child_name, parent_value=value, graph=graph)

    return graph

# Chạy chức năng xây dựng và vẽ cây
def run():
    path = '../data/data__desicion_tree.csv'
    data = load_data(path)
    target_attribute = 'buys_computer'
    attributes = ['age', 'income', 'student', 'credit_rating']
    tree_graph = build_and_draw_tree(data, target_attribute, attributes)
    tree_graph.view()  # Hiển thị cây

# Gọi hàm run để chạy toàn bộ quá trình
run()
