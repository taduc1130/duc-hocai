import numpy as np
import skfuzzy as fuzz
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt

# Load dataset Iris
iris = load_iris()
X = iris.data  # Lấy các feature của dữ liệu

# Khởi tạo mô hình Fuzzy C-Means với số lượng cụm là 3 và hệ số m là 2 (tham số quan trọng của FCM)
cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(X.T, 3, 2, error=0.005, maxiter=1000, init=None)

# Lấy nhãn của từng điểm dữ liệu bằng cách chọn cụm có giá trị u lớn nhất
labels = np.argmax(u, axis=0)

# Visualize kết quả
plt.figure(figsize=(10, 6))

# Vẽ các điểm dữ liệu theo màu tương ứng với nhãn của cụm
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', s=50)

# Vẽ các trung tâm của các cụm
plt.scatter(cntr[:, 0], cntr[:, 1], c='red', marker='x', s=200, label='Cluster Centers')

plt.title('Fuzzy C-Means Clustering on Iris Dataset')
plt.xlabel('Sepal length (cm)')
plt.ylabel('Sepal width (cm)')
plt.legend()
plt.show()
