from sklearn.datasets import load_iris
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

# Load dataset Iris
iris = load_iris()
X = iris.data  # Lấy các feature của dữ liệu
y = iris.target  # Lấy nhãn của dữ liệu (không sử dụng trong clustering)

# Khởi tạo mô hình KMeans với số lượng cụm là 3 (do Iris dataset có 3 loại hoa)
kmeans = KMeans(n_clusters=3)

# Huấn luyện mô hình trên dữ liệu
kmeans.fit(X)

# Lấy các trung tâm của các cụm
centers = kmeans.cluster_centers_

# Lấy nhãn của từng điểm dữ liệu
labels = kmeans.labels_

# Visualize kết quả
plt.figure(figsize=(10, 6))

# Vẽ các điểm dữ liệu theo màu tương ứng với nhãn của cụm
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', s=50)

# Vẽ các trung tâm của các cụm
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x', s=200, label='Cluster Centers')

plt.title('KMeans Clustering on Iris Dataset')
plt.xlabel('Sepal length (cm)')
plt.ylabel('Sepal width (cm)')
plt.legend()
plt.show()
