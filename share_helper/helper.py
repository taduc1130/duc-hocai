import csv

def read_csv(path):
    with open(path, mode='r') as file:
        reader = csv.reader(file)
        for row in reader:
            print(row)